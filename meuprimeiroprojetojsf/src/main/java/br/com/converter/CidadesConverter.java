package br.com.converter;

import java.io.Serializable;

import javax.enterprise.inject.spi.CDI;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.FacesConverter;
import javax.persistence.EntityManager;

import br.com.entidades.Cidades;

@FacesConverter(forClass = Cidades.class, value = "CidadeConverter")
public class CidadesConverter implements Converter, Serializable {
	/* CDI NÃO FUNCIONA NO CONVERTER NATIVAMENTE, TEM QUE FAZER GAMBIARRA */
	private static final long serialVersionUID = 1L;

	@Override /* Retorna o objeto Inteiro */
	public Object getAsObject(FacesContext context, UIComponent component, String codigoCidade) {
		EntityManager entityManager = CDI.current().select(EntityManager.class).get();

		Cidades cidades = (Cidades) entityManager.find(Cidades.class, Long.parseLong(codigoCidade));
		return cidades;
	}

	@Override /* Retorna apenas o código em String */
	public String getAsString(FacesContext context, UIComponent component, Object cidades) {

		if (cidades == null) {
			return null;
		}

		if (cidades instanceof Cidades) {
			return ((Cidades) cidades).getId().toString();
		} else {
			return cidades.toString();
		}
	}

}
