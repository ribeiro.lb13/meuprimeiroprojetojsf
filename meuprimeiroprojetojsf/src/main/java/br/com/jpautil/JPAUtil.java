package br.com.jpautil;

import javax.enterprise.context.RequestScoped;
import javax.enterprise.inject.Produces;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

@javax.enterprise.context.ApplicationScoped
public class JPAUtil {

	private EntityManagerFactory factory;

	public JPAUtil() {
		if (factory == null) {
			factory = Persistence.createEntityManagerFactory("meuprimeiroprojetojsf");
		}
	}

	@Produces
	@RequestScoped
	public EntityManager getEntityManager() {
		return factory.createEntityManager(); /* Prove a parte de persistência */
	}

	public Object getPrimaryKey(Object entity) { /* Retorna a primary key */
		return factory.getPersistenceUnitUtil().getIdentifier(entity);
	}
}
