package br.com.curso.jsf;

import java.awt.Graphics2D;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.Serializable;
import java.net.URL;
import java.net.URLConnection;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.annotation.PostConstruct;
import javax.faces.application.FacesMessage;
import javax.faces.component.html.HtmlSelectOneMenu;
import javax.faces.context.ExternalContext;
import javax.faces.context.FacesContext;
import javax.faces.event.AjaxBehaviorEvent;
import javax.faces.event.ValueChangeEvent;
import javax.faces.model.SelectItem;
import javax.imageio.ImageIO;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.TypedQuery;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.Part;
import javax.xml.bind.DatatypeConverter;

import com.google.gson.Gson;

import br.com.DAO.DaoGeneric;
import br.com.entidades.Cidades;
import br.com.entidades.Estados;
import br.com.entidades.Pessoa;
import br.com.jpautil.JPAUtil;
import br.com.repository.IDaoPEssoa;

@javax.faces.view.ViewScoped
@Named(value = "pessoasBean")
public class PessoasBean implements Serializable {
	private static final long serialVersionUID = 1L;

	private Pessoa pessoa = new Pessoa();

	@Inject
	private DaoGeneric<Pessoa> daoGeneric;
	@Inject
	private IDaoPEssoa iDaoPessoa;
	@Inject
	private JPAUtil jpaUtil;

	private List<Pessoa> listPessoas = new ArrayList<Pessoa>();
	private List<SelectItem> estados;
	private List<SelectItem> cidades;
	private Part aqruivoFoto;

	public Part getAqruivoFoto() {
		return aqruivoFoto;
	}

	public void setAqruivoFoto(Part aqruivoFoto) {
		this.aqruivoFoto = aqruivoFoto;
	}

	public List<SelectItem> getCidades() {
		return cidades;
	}

	public void setCidades(List<SelectItem> cidades) {
		this.cidades = cidades;
	}

	public List<SelectItem> getEstados() {
		estados = iDaoPessoa.listaEstados();
		return estados;
	}

	public void setEstados(List<SelectItem> estados) {
		this.estados = estados;
	}

	public String salvar() throws IOException {

		if (aqruivoFoto != null) {
			/* Processa Imagem */
			byte[] imagemByte = getByte(aqruivoFoto);

			/* Transforma em bufferImage */
			BufferedImage bufferedImage = ImageIO.read(new ByteArrayInputStream(imagemByte));

			/* Pega o tipo da imagem */
			int type = bufferedImage.getType() == 0 ? BufferedImage.TYPE_INT_ARGB : bufferedImage.getType();

			int largura = 200;
			int altura = 200;

			/* Criar a miniatura */
			BufferedImage resizedImage = new BufferedImage(altura, largura, type);
			Graphics2D g = resizedImage.createGraphics();
			g.drawImage(bufferedImage, 0, 0, largura, altura, null);
			g.dispose();

			/* Escrever novamente a imagem em tamanho menor */
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			String extensao = aqruivoFoto.getContentType().split("\\/")[1];
			ImageIO.write(resizedImage, extensao, baos);

			String miniImagem = "data:" + aqruivoFoto.getContentType() + ";base64,"
					+ DatatypeConverter.printBase64Binary(baos.toByteArray());

			pessoa.setFotoIconBase64(miniImagem);
			pessoa.setExtensao(extensao);
			pessoa.setFotoIconBase64Original(imagemByte);
		}

		pessoa = daoGeneric.merge(pessoa);
		carregarPessoas();
		mostrarMSG("Cadastrado com sucesso");
		return "";
	}

	public void registraLog() {

		/* Exemplo de método, actionListener é invocado antes do action */
	}

	public void mudancaDeValor(ValueChangeEvent event) {
		/* Chama na hora de salvar */
		event.getOldValue();
		event.getNewValue();
	}

	public void download() throws IOException {
		Map<String, String> params = FacesContext.getCurrentInstance().getExternalContext().getRequestParameterMap();
		String fileDownloadId = params.get("fileDownloadId");

		Pessoa pessoa = daoGeneric.consultar(Pessoa.class, fileDownloadId);
		HttpServletResponse response = (HttpServletResponse) FacesContext.getCurrentInstance().getExternalContext()
				.getResponse();

		response.addHeader("Content-Disposition", "attachmente); filename=download." + pessoa.getExtensao());
		response.setContentType("application/octet-stream");
		response.setContentLengthLong(pessoa.getFotoIconBase64Original().length);
		response.getOutputStream().write(pessoa.getFotoIconBase64Original());
		response.getOutputStream().flush();

		FacesContext.getCurrentInstance().getResponseComplete();
	}

	private byte[] getByte(Part part) throws IOException {

		byte[] bytes;
		bytes = org.apache.commons.io.IOUtils.toByteArray(part.getInputStream());
		return bytes;

	}

	public void carregaCidade(AjaxBehaviorEvent event) {
		Estados estado = (Estados) ((HtmlSelectOneMenu) event.getSource()).getValue();
		if (estado != null) {
			pessoa.setEstado(estado);
			TypedQuery cidadesQuary = jpaUtil.getEntityManager()
					.createQuery("from Cidades where estados.id = " + estado.getId(), Cidades.class);
			List<SelectItem> selectItemsCidade = new ArrayList<SelectItem>();
			List<Cidades> listCidades = cidadesQuary.getResultList();

			listCidades.forEach(i -> selectItemsCidade.add(new SelectItem(i, i.getNome())));

			setCidades(selectItemsCidade);
		}

	}

	private void mostrarMSG(String msg) {
		FacesContext context = FacesContext.getCurrentInstance();
		FacesMessage facesMessage = new FacesMessage(msg);
		context.addMessage(null, facesMessage);
	}

	public String editar() {
		if (pessoa.getCidade() != null) {
			Estados estado = pessoa.getCidade().getEstados();
			pessoa.setEstado(estado);

			TypedQuery<Cidades> cidadesQuary = jpaUtil.getEntityManager()
					.createQuery("from Cidades where estados.id = " + estado.getId(), Cidades.class);
			List<SelectItem> selectItemsCidade = new ArrayList<SelectItem>();
			List<Cidades> listCidades = cidadesQuary.getResultList();

			listCidades.forEach(i -> selectItemsCidade.add(new SelectItem(i, i.getNome())));

			setCidades(selectItemsCidade);
		}
		return "";

	}

	public void pesquisaCep() {

		// System.out.println("TESTE" + endereco.getCep());

		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
		Pessoa pessoaUser = (Pessoa) externalContext.getSessionMap().get("usuarioLogado");
		try {
			URL url = new URL("https://viacep.com.br/ws/" + pessoa.getCep() + "/json/");
			URLConnection connection = url.openConnection();
			InputStream is = connection.getInputStream();
			BufferedReader br = new BufferedReader(new java.io.InputStreamReader(is, "UTF-8"));

			String cep = "";
			StringBuilder jasonCep = new StringBuilder();
			while ((cep = br.readLine()) != null) {
				jasonCep.append(cep);
			}

			Pessoa gsonAux = new Gson().fromJson(jasonCep.toString(), Pessoa.class);
			pessoa.setCep(gsonAux.getCep());
			pessoa.setLogradouro(gsonAux.getLogradouro());
			pessoa.setComplemento(gsonAux.getComplemento());
			pessoa.setBairro(gsonAux.getBairro());
			pessoa.setLocalidade(gsonAux.getLocalidade());
			pessoa.setUf(gsonAux.getUf());
			pessoa.setIbge(gsonAux.getIbge());
			pessoa.setGia(gsonAux.getGia());

		} catch (Exception e) {
			e.printStackTrace();
			mostrarMSG("erro ao consultar o cep");
		}
	}

	public String logOut() {
		/* remove user in session */
		FacesContext context = FacesContext.getCurrentInstance();
		context.getExternalContext().invalidateSession();

		/*
		 * METODO ALEX ExternalContext externalContext = context.getExternalContext();
		 * externalContext.getSessionMap().remove("usuariologado");
		 * 
		 * Obtem o controle da sessão do usuário HttpServletRequest httpServletRequest =
		 * (HttpServletRequest) context.getCurrentInstance().getExternalContext()
		 * .getRequest();
		 * 
		 * Invalida a sessao do usuario httpServletRequest.getSession().invalidate();
		 */
		return "index.jsf?faces-redirect=true";
	}

	public String novo() {
		pessoa = new Pessoa();
		return "";
	}

	public String limpar() {
		pessoa = new Pessoa();
		return "";
	}

	/*
	 * public void pesquisaCep(AjaxBehaviorEvent event) { try { URL url = new
	 * URL("https://viacep.com.br/ws/" + pessoa.getCep() + "/json/"); URLConnection
	 * connection = url.openConnection(); InputStream is =
	 * connection.getInputStream(); BufferedReader br = new BufferedReader(new
	 * java.io.InputStreamReader(is, "UTF-8"));
	 * 
	 * String cep = ""; StringBuilder jasonCep = new StringBuilder(); while ((cep =
	 * br.readLine()) != null) { jasonCep.append(cep); } } catch (Exception e) {
	 * e.printStackTrace(); mostrarMSG("erro ao consultar o cep"); } }
	 */

	public String remove() {
		daoGeneric.deletePorId(pessoa);
		pessoa = new Pessoa();
		carregarPessoas();
		mostrarMSG("Removido com suceso");
		return "";
	}

	@PostConstruct
	public void carregarPessoas() {
		listPessoas = daoGeneric.getListEntity(Pessoa.class);

	}

	public IDaoPEssoa getiDaoPessoa() {
		return iDaoPessoa;
	}

	public void setiDaoPessoa(IDaoPEssoa iDaoPessoa) {
		this.iDaoPessoa = iDaoPessoa;
	}

	public Pessoa getPessoa() {
		return pessoa;
	}

	public void setPessoa(Pessoa pessoa) {
		this.pessoa = pessoa;
	}

	public DaoGeneric<Pessoa> getDaoGeneric() {
		return daoGeneric;
	}

	public void setDaoGeneric(DaoGeneric<Pessoa> daoGeneric) {
		this.daoGeneric = daoGeneric;
	}

	public List<Pessoa> getListPessoas() {
		return listPessoas;
	}

	public void setListPessoas(List<Pessoa> listPessoas) {
		this.listPessoas = listPessoas;
	}

	public String logar() {
		String login = pessoa.getLogin();
		String senha = pessoa.getSenha();

		Pessoa pessoaUser = iDaoPessoa.consultarUsuario(login, senha);

		if (pessoaUser != null) {// achou o usuario
			// adicionar o usuario na sessão usuarioLogado

			FacesContext context = FacesContext.getCurrentInstance();
			ExternalContext externalContext = context.getExternalContext();
			externalContext.getSessionMap().put("usuarioLogado", pessoaUser);

			return "primeirapagina.jsf";
		}

		return "index.jsf";
	}

	public boolean permiteAcesso(String acesso) {
		FacesContext context = FacesContext.getCurrentInstance();
		ExternalContext externalContext = context.getExternalContext();
		Pessoa pessoa2 = (Pessoa) externalContext.getSessionMap().get("usuarioLogado");

		return pessoa2.getPerfilUser().equals(acesso);

	}
}
