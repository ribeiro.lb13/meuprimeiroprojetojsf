package br.com.repository;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;

import javax.faces.model.SelectItem;
import javax.inject.Inject;
import javax.inject.Named;
import javax.persistence.EntityManager;
import javax.persistence.EntityTransaction;
import javax.persistence.TypedQuery;

import br.com.entidades.Estados;
import br.com.entidades.Pessoa;

@Named
public class IdaoPessoaImpl implements IDaoPEssoa, Serializable {

	private static final long serialVersionUID = 1L;

	@Inject
	private EntityManager entityManager;

	@Override
	public Pessoa consultarUsuario(String login, String senha) {
		Pessoa pessoa = null;

		EntityTransaction entityTransaction = entityManager.getTransaction();
		entityTransaction.begin();
		pessoa = (Pessoa) entityManager
				.createQuery("select p from Pessoa p where p.login = '" + login + "' and p.senha = '" + senha + "'")
				.getSingleResult();
		entityTransaction.commit();

		return pessoa;
	}

	@Override
	public List<SelectItem> listaEstados() {
		List<SelectItem> listSelecItem = new ArrayList<SelectItem>();


		TypedQuery estadosQuery = entityManager.createQuery("from Estados", Estados.class);
		List<Estados> listaEstados = estadosQuery.getResultList();

		listaEstados.forEach(i -> listSelecItem.add(new SelectItem(i, i.getNome())));

		return listSelecItem;
	}

}
